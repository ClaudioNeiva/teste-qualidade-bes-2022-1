package br.ucsal._20221.testequalidade.aula14;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class InvestingComPageObject {

	private static final String URL = "http://br.investing.com";

	private WebElement pesquisarNoSiteInput;

	public InvestingComPageObject(WebDriver driver) {
		driver.get(URL);
		pesquisarNoSiteInput = driver.findElement(By.className("searchText"));
	}

	public WebElement getPesquisarNoSiteInput() {
		return pesquisarNoSiteInput;
	}

}
