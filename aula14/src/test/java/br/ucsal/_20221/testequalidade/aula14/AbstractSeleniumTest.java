package br.ucsal._20221.testequalidade.aula14;

import java.util.stream.Stream;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.params.provider.Arguments;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public abstract class AbstractSeleniumTest {

	protected static final int TEMPO_MAXIMO_WAIT = 5;
	private static final String DRIVER_PATH = "./drivers/";
	private static WebDriver driverFirefox;
	private static WebDriver driverChrome;

	@AfterAll
	public static void teardown() {
		getDriverFirefox().close();
		getDriverChrome().close();
	}

	public static Stream<Arguments> obterDriversTeste() {
		return Stream.of(
			Arguments.of(getDriverFirefox()), 
			Arguments.of(getDriverChrome())
		);
	}

	private static WebDriver getDriverFirefox() {
		System.setProperty("webdriver.gecko.driver", DRIVER_PATH + "geckodriver_0.31.exe");
		// System.setProperty("webdriver.firefox.bin", "path/firefox.exe");
		if (driverFirefox == null) {
			driverFirefox = new FirefoxDriver();
		}
		return driverFirefox;
	}

	private static WebDriver getDriverChrome() {
		System.setProperty("webdriver.chrome.driver", DRIVER_PATH + "chromedriver_101_0.exe");
		if (driverChrome == null) {
			driverChrome = new ChromeDriver();
		}
		return driverChrome;
	}

}
