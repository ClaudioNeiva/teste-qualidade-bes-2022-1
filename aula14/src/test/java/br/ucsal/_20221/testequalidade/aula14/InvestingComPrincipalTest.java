package br.ucsal._20221.testequalidade.aula14;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class InvestingComPrincipalTest {

	private static final int TEMPO_MAXIMO_WAIT = 5;
	private static final String DRIVER_PATH = "./drivers/";
	private static WebDriver driver;
	private static WebDriverWait wait;
	
	@BeforeAll
	public static void setup() {
		setupChrome();
//		setupFirefox();
		wait = new WebDriverWait(driver, TEMPO_MAXIMO_WAIT);
	}

	private static void setupFirefox() {
		System.setProperty("webdriver.gecko.driver",DRIVER_PATH+"geckodriver_0.31.exe");
		// System.setProperty("webdriver.firefox.bin", "path/firefox.exe");
		driver = new FirefoxDriver();
	}

	private static void setupChrome() {
		System.setProperty("webdriver.chrome.driver", DRIVER_PATH+"chromedriver_101_0.exe");
		driver = new ChromeDriver();
	}

	@AfterAll
	public static void teardown() {
		driver.close();
	}

	@Test
	public void testarPesquisa() throws InterruptedException {

		// Abrir página do Investing.com
		driver.get("http://br.investing.com");

		// Preencher o input de "Pesquisar no site..."
		WebElement pesquisarNoSiteInput = driver.findElement(By.className("searchText"));
		pesquisarNoSiteInput.sendKeys("COGN3" + Keys.ENTER);

		// Obter o conteúdo da página
		// Thread.sleep(5000);
		wait.until(ExpectedConditions.numberOfElementsToBe(By.className("js-inner-all-results-quotes-wrapper"), 1));
		String conteudo = driver.getPageSource();

		// Verificar se retorno inclui "Cogna Educação"
		Assertions.assertTrue(conteudo.contains("Cogna Educacao"));
	}

}
