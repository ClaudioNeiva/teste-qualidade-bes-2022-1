package br.ucsal.testequalidade20221.locadora.persistence;

import java.util.ArrayList;
import java.util.List;

import br.ucsal.testequalidade20221.locadora.dominio.Cliente;
import br.ucsal.testequalidade20221.locadora.exception.ClienteNaoEncontradoException;

public class ClienteDAO {

	private List<Cliente> clientes = new ArrayList<>();

	public Cliente obterPorCpf(String cpf) throws ClienteNaoEncontradoException {
		for (Cliente cliente : clientes) {
			if (cliente.getCpf().equalsIgnoreCase(cpf)) {
				return cliente;
			}
		}
		throw new ClienteNaoEncontradoException();
	}
	
	public void insert(Cliente cliente){
		clientes.add(cliente);
	}

}
