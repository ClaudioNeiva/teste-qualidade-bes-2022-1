package br.ucsal.testequalidade20221.locadora.dominio.enums;

public enum SituacaoVeiculoEnum {
	DISPONIVEL, MANUTENCAO, LOCADO;
}
