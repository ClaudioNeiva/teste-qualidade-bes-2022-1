package br.ucsal.testequalidade20221.locadora.mock;

import java.util.List;

import br.ucsal.testequalidade20221.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20221.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.testequalidade20221.locadora.persistence.VeiculoDAO;

public class VeiculoDAOStub extends VeiculoDAO {

	private List<Veiculo> veiculosTest;

	public void setup(List<Veiculo> veiculosTest) {
		this.veiculosTest = veiculosTest;
	}

	@Override
	public List<Veiculo> obterPorPlacas(List<String> placas) throws VeiculoNaoEncontradoException {
		return veiculosTest;
	}

}
