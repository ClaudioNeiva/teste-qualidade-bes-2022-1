package br.ucsal.testequalidade20221.locadora.builder;

import br.ucsal.testequalidade20221.locadora.dominio.Modelo;
import br.ucsal.testequalidade20221.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20221.locadora.dominio.enums.SituacaoVeiculoEnum;

public class VeiculoBuilder {

	private static final String DEFAULT_PLACA = "BCD-3456";
	private static final Integer DEFAULT_ANO_FABRICACAO = null;
	private static final Modelo DEFAULT_MODELO = new Modelo("Gol");
	private static final Double DEFAULT_VALOR_DIARIA = null;
	private static final SituacaoVeiculoEnum DEFAULT_SITUACAO = null;

	private String placa = DEFAULT_PLACA;
	private Integer anoFabricacao = DEFAULT_ANO_FABRICACAO;
	private Modelo modelo = DEFAULT_MODELO;
	private Double valorDiaria = DEFAULT_VALOR_DIARIA;
	private SituacaoVeiculoEnum situacao = DEFAULT_SITUACAO;

	private VeiculoBuilder() {
	}

	public static VeiculoBuilder umVeiculo() {
		return new VeiculoBuilder();
	}

	public static VeiculoBuilder umVeiculoDisponivel() {
		return new VeiculoBuilder().disponivel();
	}

	public VeiculoBuilder comPlaca(String placa) {
		this.placa = placa;
		return this;
	}

	public VeiculoBuilder fabricadoEm(Integer anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
		return this;
	}

	public VeiculoBuilder doModelo(String modelo) {
		this.modelo = new Modelo(modelo);
		return this;
	}

	public VeiculoBuilder doModelo(Modelo modelo) {
		this.modelo = modelo;
		return this;
	}

	public VeiculoBuilder comValorDiaria(Double valorDiaria) {
		this.valorDiaria = valorDiaria;
		return this;
	}

	public VeiculoBuilder disponivel() {
		this.situacao = SituacaoVeiculoEnum.DISPONIVEL;
		return this;
	}

	public VeiculoBuilder locado() {
		this.situacao = SituacaoVeiculoEnum.LOCADO;
		return this;
	}

	public VeiculoBuilder emManutencao() {
		this.situacao = SituacaoVeiculoEnum.MANUTENCAO;
		return this;
	}

	public VeiculoBuilder comSituacao(SituacaoVeiculoEnum situacao) {
		this.situacao = situacao;
		return this;
	}

	public VeiculoBuilder mas() {
		return new VeiculoBuilder().comPlaca(placa).fabricadoEm(anoFabricacao).doModelo(modelo)
				.comValorDiaria(valorDiaria).comSituacao(situacao);
	}

	public Veiculo build() {
		Veiculo veiculo = new Veiculo();
		veiculo.setPlaca(placa);
		veiculo.setAnoFabricacao(anoFabricacao);
		veiculo.setModelo(modelo);
		veiculo.setValorDiaria(valorDiaria);
		veiculo.setSituacao(situacao);
		return veiculo;
	}

}
