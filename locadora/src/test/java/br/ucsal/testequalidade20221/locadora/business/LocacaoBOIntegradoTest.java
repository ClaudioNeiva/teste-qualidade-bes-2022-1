package br.ucsal.testequalidade20221.locadora.business;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import br.ucsal.testequalidade20221.locadora.dominio.Modelo;
import br.ucsal.testequalidade20221.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20221.locadora.dominio.enums.SituacaoVeiculoEnum;
import br.ucsal.testequalidade20221.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.testequalidade20221.locadora.persistence.VeiculoDAO;

/**
 * Testes para os métodos da classe LocacaoBO.
 * 
 * @author claudioneiva
 *
 */
class LocacaoBOIntegradoTest {
	
	/**
	 * Testar o cálculo do valor total de um contrato de locação com 5 dias de
	 * duração, 4 veículos locados, 3 fabricados há 2 anos e 1 fabricado há 15 anos.
	 * @throws VeiculoNaoEncontradoException 
	 */

	private VeiculoDAO veiculoDAO;
	private LocacaoBO locacaoBO;
	
	@BeforeEach
	void setup() {
		veiculoDAO = new VeiculoDAO();
		locacaoBO = new LocacaoBO(veiculoDAO);
	}
	
	@Test
	void testarCalculoValorTotalLocacao4Veiculo5Dias() throws VeiculoNaoEncontradoException {
		int quantidadeDiasLocacao = 5;
		
		Veiculo veiculo1 = criarVeiculoFabricadoHa2Anos("EDF3456");
		Veiculo veiculo2 = criarVeiculoFabricadoHa2Anos("QSD4578");
		Veiculo veiculo3 = criarVeiculoFabricadoHa2Anos("FTU6422");
		Veiculo veiculo4 = criarVeiculoFabricadoHa15Anos("SAE2459");
		
		List<String> placas = Arrays.asList(veiculo1.getPlaca(),veiculo2.getPlaca(),veiculo3.getPlaca(),veiculo4.getPlaca());
		Double valorEsperadoLocacao = 3675d;
		
		veiculoDAO.insert(veiculo1);
		veiculoDAO.insert(veiculo2);
		veiculoDAO.insert(veiculo3);
		veiculoDAO.insert(veiculo4);
		
		Double valorLocacaoAtual = locacaoBO.calcularValorTotalLocacao(placas, quantidadeDiasLocacao, LocalDate.now());
		Assertions.assertEquals(valorEsperadoLocacao, valorLocacaoAtual);
	}
	
	private Veiculo criarVeiculoFabricadoHa2Anos(String placa) {
		Integer anoFabricacaoHa2Anos = LocalDate.now().getYear() - 2;
		Modelo modelo = new Modelo("Nissan");
		Double valorDiaria = 200d;
		return criarVeiculo(placa, modelo, anoFabricacaoHa2Anos, valorDiaria);
	}

	private Veiculo criarVeiculoFabricadoHa15Anos(String placa) {
		Integer anoFabricacaoHa15Anos = LocalDate.now().getYear() - 15;
		Modelo modelo = new Modelo("Volvo");
		Double valorDiaria = 150d;
		return criarVeiculo(placa, modelo, anoFabricacaoHa15Anos, valorDiaria);
	}
	
	private Veiculo criarVeiculo(String placa, Modelo modelo, Integer anoFabricacaoHa2Anos, Double valorDiaria) {
		Veiculo veiculo = new Veiculo();
		veiculo.setPlaca(placa);
		veiculo.setAnoFabricacao(anoFabricacaoHa2Anos);
		veiculo.setModelo(modelo);
		veiculo.setValorDiaria(valorDiaria);
		veiculo.setSituacao(SituacaoVeiculoEnum.DISPONIVEL);
		return veiculo;
	}
}
