package br.ucsal.testequalidade20221.locadora.builder;

import java.time.LocalDate;

import br.ucsal.testequalidade20221.locadora.dominio.Modelo;
import br.ucsal.testequalidade20221.locadora.dominio.Veiculo;

public class VeiculoMother {

	private static Modelo modelo = new Modelo("Etios");

	public static Veiculo umVeiculoNovoDisponivelBarato() {
		Veiculo veiculo = new Veiculo();
		veiculo.setPlaca("PQW-5957");
		veiculo.setAnoFabricacao(LocalDate.now().getYear() - 2);
		veiculo.setModelo(modelo);
		veiculo.setValorDiaria(100.00);
		veiculo.setAnoFabricacao(LocalDate.now().getYear() - 2);
		return veiculo;
	}

	public static Veiculo umVeiculoNovoDisponivelCaro() {
		Veiculo veiculo = new Veiculo();
		veiculo.setPlaca("PQW-5957");
		veiculo.setAnoFabricacao(LocalDate.now().getYear() - 2);
		veiculo.setModelo(modelo);
		veiculo.setValorDiaria(400.00);
		veiculo.setAnoFabricacao(LocalDate.now().getYear() - 2);
		return veiculo;
	}

	public static Veiculo umVeiculoAntigoDisponivel() {
		Veiculo veiculo = new Veiculo();
		veiculo.setPlaca("PQW-5957");
		veiculo.setAnoFabricacao(LocalDate.now().getYear() - 15);
		veiculo.setModelo(modelo);
		veiculo.setValorDiaria(149.99);
		veiculo.setAnoFabricacao(LocalDate.now().getYear() - 2);
		return veiculo;
	}

}
