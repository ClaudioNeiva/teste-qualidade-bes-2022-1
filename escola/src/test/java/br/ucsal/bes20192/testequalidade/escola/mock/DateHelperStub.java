package br.ucsal.bes20192.testequalidade.escola.mock;

import br.ucsal.bes20192.testequalidade.escola.util.DateHelper;

public class DateHelperStub extends DateHelper {

	private Integer anoAtualTest;

	public void setup(Integer anoAtualTest) {
		this.anoAtualTest = anoAtualTest;
	}

	@Override
	public Integer obterAnoAtual() {
		return anoAtualTest;
	}

}
