package br.ucsal.bes20192.testequalidade.escola.mock;

import java.util.HashMap;

import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;

public class AlunoDAOFake {

	private HashMap<Integer, Aluno> alunos = new HashMap<>();

	public void salvar(Aluno aluno) {
		alunos.put(aluno.getMatricula(), aluno);
	}

	public void excluirTodos() {
		alunos.clear();
	}

	public Aluno encontrarPorMatricula(Integer matricula) {
		return alunos.get(matricula);
	}

}
