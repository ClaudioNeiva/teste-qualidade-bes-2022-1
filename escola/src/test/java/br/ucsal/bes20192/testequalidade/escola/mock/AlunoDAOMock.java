package br.ucsal.bes20192.testequalidade.escola.mock;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Assertions;

import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;

public class AlunoDAOMock extends AlunoDAO {

	private Aluno alunoTest;

	private Map<Aluno, Integer> qtdChamadasSalvar = new HashMap<>();

	public void setup(Aluno alunoTest) {
		this.alunoTest = alunoTest;
	}

	@Override
	public Aluno encontrarPorMatricula(Integer matricula) {
		return alunoTest;
	}

	@Override
	public void salvar(Aluno aluno) {
		qtdChamadasSalvar.putIfAbsent(aluno, 0);
		qtdChamadasSalvar.put(aluno, qtdChamadasSalvar.get(aluno) + 1);
	}

	public void verificarChamadasSalvar(Aluno alunoEsperado, Integer qtdEsperada) {
		if (!qtdChamadasSalvar.containsKey(alunoEsperado)) {
			Assertions.fail("Não foi feita chamada do método salvar para o alunoEsperado.");
		}
		if (!qtdChamadasSalvar.get(alunoEsperado).equals(qtdEsperada)) {
			Assertions.fail("A quantidade de chamadas esperadas para salvar é " + qtdEsperada + " mas foram feitas "
					+ qtdChamadasSalvar.get(alunoEsperado) + " chamadas.");
		}
	}

}
