package br.ucsal.bes20192.testequalidade.escola.business;

import java.time.LocalDate;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import br.ucsal.bes20192.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20192.testequalidade.escola.util.DateHelper;

@TestInstance(Lifecycle.PER_CLASS)
class AlunoBOIntegradoTest {

	private AlunoDAO alunoDAO;
	private DateHelper dateUtil;
	private AlunoBO alunoBO;

	@BeforeAll
	void setupAll() {
		alunoDAO = new AlunoDAO();
		dateUtil = new DateHelper();
		alunoBO = new AlunoBO(alunoDAO, dateUtil);
	}

	@BeforeEach
	void setup() {
		alunoDAO.excluirTodos();
	}

	@AfterAll
	void tearDownAll() {
		alunoDAO.excluirTodos();
	}

	/**
	 * Verificar se um aluno nascido em há 16 anos, tem sua idade corretamente
	 * calculada.
	 */
	@Test
	@DisplayName("Calcular idade de aluno nascido há 16 anos")
	public void testarCalculoIdadeAluno1() {
		Integer matricula = 100;
		Integer anoHa16Anos = LocalDate.now().getYear() - 16;

		Aluno aluno1 = AlunoBuilder.umAlunoAtivo().comMatricula(matricula).nascidoEm(anoHa16Anos).build();
		alunoDAO.salvar(aluno1);

		Integer idadeEsperada = 16;
		Integer idadeAtual = alunoBO.calcularIdade(matricula);

		Assertions.assertEquals(idadeEsperada, idadeAtual);
	}

	/**
	 * Verificar se alunos ativos são atualizados.
	 */
	@Test
	public void testarAtualizacaoAlunosAtivos() {
		Integer matricula = 100;
		Aluno alunoEsperado = AlunoBuilder.umAlunoAtivo().comMatricula(matricula).build();

		alunoBO.atualizar(alunoEsperado);
		Aluno alunoAtual = alunoDAO.encontrarPorMatricula(matricula);
		
		Assertions.assertEquals(alunoEsperado, alunoAtual);
	}

}
