package br.ucsal.bes20192.testequalidade.escola.tui;

import java.util.Scanner;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.Mockito;

@TestInstance(Lifecycle.PER_CLASS)
public class TuiHelperTest {

	private TuiHelper tuiHelper;
	private Scanner scannerMock;

	@BeforeAll
	void setup() {
		tuiHelper = new TuiHelper();
		scannerMock = Mockito.mock(Scanner.class);
		tuiHelper.scanner = scannerMock;
	}
	
	@Test
	void testarObtencaoNomeCompleto() {
		String nome = "Claudio";
		String sobrenome = "Neiva";
		Mockito.when(scannerMock.nextLine()).thenReturn(nome).thenReturn(sobrenome);

		String nomeCompletoEsperado = "Claudio Neiva";
		String nomeCompletoAtual = tuiHelper.obterNomeCompleto();

		Assertions.assertEquals(nomeCompletoEsperado, nomeCompletoAtual);
	}

}
