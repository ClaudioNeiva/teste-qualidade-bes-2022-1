package br.ucsal.bes20192.testequalidade.escola.business;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import br.ucsal.bes20192.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.mock.AlunoDAOMock;
import br.ucsal.bes20192.testequalidade.escola.mock.DateHelperStub;

@TestInstance(Lifecycle.PER_CLASS)
class AlunoBOMockCodificadoTest {

	private AlunoDAOMock alunoDAOMock;
	private DateHelperStub dateHelpStub;
	private AlunoBO alunoBO;

	@BeforeAll
	void setupAll() {
		alunoDAOMock = new AlunoDAOMock();
		dateHelpStub = new DateHelperStub();
		alunoBO = new AlunoBO(alunoDAOMock, dateHelpStub);
	}

	/**
	 * Verificar se um aluno nascido em em 2003, terá 16 anos de idade em 2019.
	 */
	@Test
	@DisplayName("Calcular idade de aluno nascido em 2003")
	public void testarCalculoIdadeAluno1() {
		Integer matricula = 100;

		Aluno aluno1 = AlunoBuilder.umAlunoAtivo().comMatricula(matricula).nascidoEm(2003).build();
		alunoDAOMock.setup(aluno1);

		dateHelpStub.setup(2019);

		Integer idadeEsperada = 16;
		Integer idadeAtual = alunoBO.calcularIdade(matricula);

		Assertions.assertEquals(idadeEsperada, idadeAtual);
	}

	/**
	 * Verificar se alunos ativos sao atualizados.
	 */
	@Test
	public void testarAtualizacaoAlunosAtivos() {
		Integer matricula = 100;
		Aluno alunoEsperado = AlunoBuilder.umAlunoAtivo().comMatricula(matricula).build();

		alunoBO.atualizar(alunoEsperado);

		alunoDAOMock.verificarChamadasSalvar(alunoEsperado, 1);
	}

}
