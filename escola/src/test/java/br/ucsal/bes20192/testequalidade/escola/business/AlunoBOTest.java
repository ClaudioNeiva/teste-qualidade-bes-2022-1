package br.ucsal.bes20192.testequalidade.escola.business;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.Mockito;

import br.ucsal.bes20192.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20192.testequalidade.escola.util.DateHelper;

@TestInstance(Lifecycle.PER_CLASS)
public class AlunoBOTest {

	private AlunoBO alunoBO;
	private AlunoDAO alunoDAOMock;
	private DateHelper dateHelperMock;

	@BeforeAll
	void setup() {
		alunoDAOMock = Mockito.mock(AlunoDAO.class);
		dateHelperMock = Mockito.mock(DateHelper.class);
		this.alunoBO = new AlunoBO(alunoDAOMock, dateHelperMock);
	}

	@Test
	public void testarAtualizacaoAlunosAtivos() {
		Integer matricula = 100;
		Aluno alunoEsperado = AlunoBuilder.umAlunoAtivo().comMatricula(matricula).build();

		alunoBO.atualizar(alunoEsperado);

		Mockito.verify(alunoDAOMock).salvar(alunoEsperado);
	}

}
