package br.ucsal.bes20192.testequalidade.escola.business;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import br.ucsal.bes20192.testequalidade.escola.builder.AlunoBuilder;
import br.ucsal.bes20192.testequalidade.escola.domain.Aluno;
import br.ucsal.bes20192.testequalidade.escola.persistence.AlunoDAO;
import br.ucsal.bes20192.testequalidade.escola.util.DateHelper;

@TestInstance(Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension.class)
public class AlunoBO2Test {

	@Mock
	private AlunoDAO alunoDAOMock;

	@Mock
	private DateHelper dateHelperMock;
	
	@InjectMocks
	private AlunoBO alunoBO;

	@BeforeAll
	void setup() {
		this.alunoBO = new AlunoBO();
	}

	@Test
	public void testarAtualizacaoAlunosAtivos() {
		Integer matricula = 100;
		Aluno alunoEsperado = AlunoBuilder.umAlunoAtivo().comMatricula(matricula).build();

		alunoBO.atualizar(alunoEsperado);

		Mockito.verify(alunoDAOMock).salvar(alunoEsperado);
	}

}
