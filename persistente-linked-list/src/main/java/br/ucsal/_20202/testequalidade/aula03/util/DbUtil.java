package br.ucsal._20202.testequalidade.aula03.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbUtil {

	private static final String URL = "jdbc:postgresql://localhost:5432/teste-qualidade-bes-2022-1";
	private static final String USER = "postgres";
	private static final String PASSWORD = "postgresql";

	private static Connection connection;

	private DbUtil() {
	}
	
	public static Connection getConnection() throws SQLException {
		if (connection == null) {
			connection = DriverManager.getConnection(URL, USER, PASSWORD);
		}
		return connection;
	}

}
