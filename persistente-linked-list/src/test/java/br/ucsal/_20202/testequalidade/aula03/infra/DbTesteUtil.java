package br.ucsal._20202.testequalidade.aula03.infra;

import java.sql.SQLException;

import br.ucsal._20202.testequalidade.aula03.util.DbUtil;

public class DbTesteUtil {
	
	private DbTesteUtil() {
	}

	public static Boolean isConnectionValid() {
		try {
			DbUtil.getConnection();
			return true;
		} catch (SQLException e) {
			return false;
		}
	}
}
