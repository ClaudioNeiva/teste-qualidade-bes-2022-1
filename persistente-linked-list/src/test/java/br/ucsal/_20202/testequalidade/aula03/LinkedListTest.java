package br.ucsal._20202.testequalidade.aula03;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import br.ucsal._20202.testequalidade.aula03.exception.InvalidElementException;
import br.ucsal._20202.testequalidade.aula03.interfaces.List;

class LinkedListTest {

	private List<String> nomes;
	
	@BeforeEach
	void setup() {
		nomes = new LinkedList<>();
	}
	
	@Test
	@Disabled
	void testarAdd1Nome() throws InvalidElementException {
		// Dados de entrada
		String nomeEntrada = "claudio";

		// Saída esperada
		String nomeEsperado = "claudio";

		// Executar o método que está sendo testado
		nomes.add(nomeEntrada);

		// Obter o saída atual
		String nomeAtual = nomes.get(0);

		// Comparar a saída esperada com a saída atual
		assertEquals(nomeEsperado, nomeAtual);
	}

	@Test
	@DisplayName("Testar a adição de 3 nomes à lista.")
	void testarAdd3Nomes() throws InvalidElementException {
		// Dados de entrada
		String nomeEntrada1 = "claudio";
		String nomeEntrada2 = "antonio";
		String nomeEntrada3 = "neiva";

		// Saída esperada
		String nomeEsperado1 = "claudio";
		String nomeEsperado2 = "antonio";
		String nomeEsperado3 = "neiva";

		// Executar o método que está sendo testado
		nomes.add(nomeEntrada1);
		nomes.add(nomeEntrada2);
		nomes.add(nomeEntrada3);

		// Obter o saída atual
		String nomeAtual1 = nomes.get(0);
		String nomeAtual2 = nomes.get(1);
		String nomeAtual3 = nomes.get(2);

		// Comparar a saída esperada com a saída atual
		Assertions.assertAll(
				() -> assertEquals(nomeEsperado1, nomeAtual1),
				() -> assertEquals(nomeEsperado2, nomeAtual2), 
				() -> assertEquals(nomeEsperado3, nomeAtual3));
	}

	@Test
	void testarAddValorInvalido() {
		String mensagemEsperada = "Elemento não válido.";
		InvalidElementException exceptionAtual = Assertions.assertThrows(InvalidElementException.class,
				() -> nomes.add(null));
		Assertions.assertEquals(mensagemEsperada, exceptionAtual.getMessage());
	}


	@Test
	@DisplayName("Testar a consulta à um índice inválido.")
	void testarGetNome() throws InvalidElementException {
		nomes.add("claudio");
		nomes.add("antonio");
		Assertions.assertNull(nomes.get(2));
	}

	@Test
	void testarSize() throws InvalidElementException {
		nomes.add("claudio");
		nomes.add("antonio");
		Assertions.assertEquals(2, nomes.size());
	}

	@Test
	void testarClear() throws InvalidElementException {
		nomes.add("claudio");
		nomes.add("antonio");
		nomes.clear();
		Assertions.assertAll (
			() -> Assertions.assertEquals(0, nomes.size()),
			() -> Assertions.assertNull(nomes.get(0)),
			() -> Assertions.assertNull(nomes.get(1)));
	}

}
