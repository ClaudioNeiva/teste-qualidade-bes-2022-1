package br.ucsal._20202.testequalidade.aula03;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import br.ucsal._20202.testequalidade.aula03.exception.InvalidElementException;
import br.ucsal._20202.testequalidade.aula03.infra.DbTesteUtil;
import br.ucsal._20202.testequalidade.aula03.interfaces.PersistentList;
import br.ucsal._20202.testequalidade.aula03.util.DbUtil;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PersitenteLinkedListTest {

	private static final Long ID_LISTA = 1L;
	private static final String TAB_NAME = "lista1";
	
	private Connection connection;
	private PersistentList<String> nomes;

	@BeforeAll
	void setupClass() throws SQLException {
		Assumptions.assumeTrue(DbTesteUtil.isConnectionValid());
		connection = DbUtil.getConnection();
	}
	
	@BeforeEach
	void setup() throws SQLException {
		nomes = new PersitenteLinkedList<>();
		nomes.delete(ID_LISTA, connection, TAB_NAME);
	}
	
	@AfterAll
	void tearDownClass() throws SQLException {
		nomes.delete(ID_LISTA, connection, TAB_NAME);
		connection.close();
	}

	@Test
	void testarPersistenciaLista3Nomes()
			throws InvalidElementException, SQLException, IOException, ClassNotFoundException {
		// Dado de entrada
		nomes.add("antonio");
		nomes.add("claudio");
		nomes.add("neiva");

		// Executar o método que está sendo testado
		nomes.persist(ID_LISTA, connection, TAB_NAME);

		// Obter o resultado atual
		PersistentList<String> nomesAtual = new PersitenteLinkedList<>();
		nomesAtual.load(ID_LISTA, connection, TAB_NAME);

		// Verificar se elementos da lista atual são o mesmos da lista de entrada
		Assertions.assertAll(() -> assertEquals("antonio", nomesAtual.get(0)),
				() -> assertEquals("claudio", nomesAtual.get(1)), () -> assertEquals("neiva", nomesAtual.get(2)));

	}

	@Test
	void testarPersistencia2() {

	}

	@Test
	void testarPersistencia3() {

	}

	@Test
	void testarPersistencia4() {

	}

}
