package br.ucsal._20202.testequalidade.aula03;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ExemploAnotacoesJUnitTest {

	@BeforeAll
	static void setupClass() {
		System.out.println("setupClass");
	}

	@BeforeEach
	void setup() {
		System.out.print("	setup");
		limparBase();
	}

	@AfterEach
	void tearDown() {
		System.out.print("	tearDown");
		limparBase();
	}

	@AfterAll
	static void tearDownClass() {
		System.out.println("tearDownClass");

	}

	@Test
	void teste1() {
		System.out.println("		teste1");
	}

	@Test
	void teste2() {
		System.out.println("		teste2");
	}

	@Test
	void teste3() {
		System.out.println("		teste3");
	}

	private void limparBase() {
		System.out.println(" - Limpando a base...");
	}
	
}
