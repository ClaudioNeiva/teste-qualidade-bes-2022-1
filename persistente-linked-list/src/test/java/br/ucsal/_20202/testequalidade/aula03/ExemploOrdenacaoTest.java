package br.ucsal._20202.testequalidade.aula03;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

@TestMethodOrder(MethodOrderer.Random.class)
public class ExemploOrdenacaoTest {

	@Test
	@DisplayName("Antonio")
	@Order(1)
	void testarA() {
		System.out.println("jaca");
	}

	@Test
	@DisplayName("Neiva")
	@Order(5)
	void testarC() {
		System.out.println("manga");
	}

	@Test
	@DisplayName("Pedreira")
	@Order(2)
	void testarB() {
		System.out.println("caju");
	}

	@Test
	@DisplayName("Claudio")
	@Order(4)
	void testarD() {
		System.out.println("goiaba");
	}

	@Test
	@DisplayName("Maria")
	@Order(3)
	void testarE() {
		System.out.println("melancia");
	}

}
