package br.ucsal._20202.testequalidade.aula03.util;

import java.util.stream.Stream;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

public class CalculoUtilTest {

	@ParameterizedTest(name = "{index} - calcularFatorial({0})")
	@CsvSource({ "0,1", "1,1", "3,6", "5,120" })
	void testarFatorial(int n, long fatorialEsperado) {
		long fatorialAtual = CalculoUtil.calcularFatorial(n);
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@ParameterizedTest(name = "{index} - calcularFatorial({0})")
	@MethodSource("fornecerDadosTest")
	void testarFatorial2(int n, long fatorialEsperado) {
		long fatorialAtual = CalculoUtil.calcularFatorial(n);
		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}

	private static Stream<Arguments> fornecerDadosTest() {
		return Stream.of(Arguments.of(0, 1), Arguments.of(4, 24), Arguments.of(1, 1));
	}

}
