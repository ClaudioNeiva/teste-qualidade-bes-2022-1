package br.ucsal._20221.testequalidade.aula12;

import java.lang.reflect.Method;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.powermock.reflect.Whitebox;

public class CalculoUtilTest {

	@ParameterizedTest
	@CsvSource({ "0,1", "1,1", "3,6", "5,120" })
	void testarFatorial(int n, Long fatorialEsperado) {
		Long fatorialAtual = CalculoUtil.calcularFatorial(n);

		Assertions.assertEquals(fatorialEsperado, fatorialAtual);
	}

	@ParameterizedTest
	@CsvSource({ "4,false", "5,true" })
	void testarPrimo(int n, Boolean isPrimoEsperado) {
		Boolean isPrimoAtual = CalculoUtil.verificarPrimo(n);
		Assertions.assertEquals(isPrimoEsperado, isPrimoAtual);
	}

	@ParameterizedTest
	@CsvSource({ "4,3", "5,2" })
	void testarCalcularQtdDivisoresComReflection(int n, int qtdDivisoresEsperada) throws Exception {
		Method metodoCalcularQtdDivisores = CalculoUtil.class.getDeclaredMethod("calcularQtdDivisores", int.class);
		metodoCalcularQtdDivisores.setAccessible(true);
		int qtdDivisoresAtual = (int) metodoCalcularQtdDivisores.invoke(CalculoUtil.class, n);
		Assertions.assertEquals(qtdDivisoresEsperada, qtdDivisoresAtual);
	}

	@ParameterizedTest
	@CsvSource({ "4,3", "5,2" })
	void testarCalcularQtdDivisoresComWhitebox(int n, int qtdDivisoresEsperada) throws Exception {
		int qtdDivisoresAtual = Whitebox.invokeMethod(CalculoUtil.class, "calcularQtdDivisores", n);
		Assertions.assertEquals(qtdDivisoresEsperada, qtdDivisoresAtual);
	}

}
