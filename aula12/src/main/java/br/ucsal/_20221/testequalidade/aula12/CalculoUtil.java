package br.ucsal._20221.testequalidade.aula12;

public class CalculoUtil {

	public static Long calcularFatorial(int n) {
		if (n == 0) {
			return 1L;
		}
		return n * calcularFatorial(n - 1);
	}

	public static Boolean verificarPrimo(int n) {
		int qtdDividisores = calcularQtdDivisores(n);
		if (qtdDividisores == 2) {
			return true;
		}
		return false;
	}

	private static int calcularQtdDivisores(int n) {
		int qtdDivisores=0;
		for(int i=1;i<=n;i++) {
			if(n%i==0) {
				qtdDivisores++;
			}
		}
		return qtdDivisores;
	}

}
